# Infrastructure
## Sources
- Machines name &amp; IP addresses census: https://dev1galaxy.org/other/infra/
- Public domains census: https://git.devuan.org/devuan/nameserver

## Map
<table>
    <tr style="text-align: center">
        <th>Machine</th>
        <th>IP addresses</th>
        <th>Use</th>
        <th>Services</th>
        <th>Ports</th>
    </tr>
    <tr>
        <td rowspan="3">pkginfo</td>
        <td style="font-family: monospace">54.36.142.179/8</td>
        <td rowspan="3" style="font-family: monospace">pkginfo.devuan.org</td>
        <td rowspan="3">nginx</td>
        <td rowspan="2" style="font-family: monospace">80, 443</td>
    </tr>
    <tr>
        <td style="font-family: monospace">192.168.97.87/24</td>
    </tr>
    <tr>
        <td style="font-family: monospace">&lt;IPv6&gt;</td>
        <td style="font-family: monospace">80</td>
    </tr>
    <tr>
        <td rowspan="2">doc</td>
        <td style="font-family: monospace">54.36.142.178/32</td>
        <td rowspan="2">None</td>
        <td rowspan="2">None</td>
        <td rowspan="2" style="font-family: monospace">None</td>
    </tr>
    <tr>
        <td style="font-family: monospace">192.168.97.86/24</td>
    </tr>
    <tr>
        <td rowspan="2">?</td>
        <td style="font-family: monospace">54.36.142.177/32</td>
        <td rowspan="2">None</td>
        <td rowspan="2">None</td>
        </td><td rowspan="2" style="font-family: monospace">None</td>
    </tr>
    <tr>
        <td style="font-family: monospace">2001:41d0:2:d06e::3624:8eb1/128</td>
    </tr>
</table>
